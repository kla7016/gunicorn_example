#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from flask import Flask, request, jsonify

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'

@app.route('/')
def index():
    return "Hello World"


if __name__ == '__main__':
    app.run(debug=True,threaded=True,host='0.0.0.0',port=8034)
