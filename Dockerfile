FROM python:2.7
FROM python:2.7

COPY . /app

RUN chmod +x /app/start.sh
WORKDIR /app
RUN pip install -r requirements.txt

EXPOSE 8034

CMD /app/start.sh
